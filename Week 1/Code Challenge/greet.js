// Importing Module
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Code here!

/*
 * This function is being used to greet people
 * The result of that function should be:
 * "Hello, <name>, looks like you're <age>! And you lived in <city>!"
 *
 * HINT:
 * To get the current year, let say 2020;
 * You can use this code
 *
 * const currentDate = new Date();
 * const currentYear = currentDate.getFullYear();
 * */
function greet(name, couple, Since) {
  // Insert your code here!
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();
  const greetPeople = `Hello,${name}, looks like you really love with ${couple}! and you have love ${couple} for ${
    currentYear - Since
  } years! `;
  console.log(greetPeople);
  return greetPeople;
}
// DON'T CHANGE
console.log("Goverment Registry\n");
// GET User's Name
rl.question("What is your name? ", (name) => {
  // GET User's Couple
  rl.question("Who's name your couple? ", (couple) => {
    // GET User's Birthday
    rl.question(
      "Since when you fall in love with her/him? (ex: 2010, 2015) ",
      (Since) => {
        greet(name, couple, Since);

        rl.close();
      }
    );
  });
});

rl.on("close", () => {
  process.exit();
});
