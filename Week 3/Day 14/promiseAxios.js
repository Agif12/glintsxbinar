const axios = require ("axios");

let urlUserPublisher = "https://gist.githubusercontent.com/andityadimas1/380de7c81ff8046b1045fc325abec4c1/raw/eb604cdcb036f295bf229117f906cb75bd392c5e/getUserPublisher.json";
let urlSubmittedArticles = "https://gist.githubusercontent.com/sahlannasution/9fb3ce93520865911b267caf044a7c5b/raw/545e9d97c9dc69dff97df3fd188992f99f778b9b/getSubmittedArticles.json";
let urlAuthorArticles = "https://gist.githubusercontent.com/sahlannasution/680bd223b440c06604fb39565fd67a9d/raw/7f8577a0a06066340bdd282f6a28ed08575ac4be/getAuthorArticles.json";
let data = {};

axios
  .get(urlUserPublisher)
  .then((response) => {

      data = {  UserPublisher: response.data.data };
      console.log(data.UserPublisher);

      return axios.get(urlSubmittedArticles);
    //   let result = response.data.data
    //   console.log(data);
  })
  .then ((response) => {
      data = { ...data, SubmittedArticles: response.data.data };
      console.log(data.SubmittedArticles);
      return axios.get(urlAuthorArticles);
  })
  .then ((response) => {
      data = { ...data, AuthorArticles: response.data.data };
      console.log(data.AuthorArticles);
  })
  .catch((err) => {
    console.log(err.message);
  });