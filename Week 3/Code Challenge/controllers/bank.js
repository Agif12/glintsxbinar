let data = require("../models/dataBank.json"); 


class Bank {
  getAllBanks(req, res, next) {
    try {
      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  getDetailBanks(req, res, next) {
    try {
      let detailData = data.filter(
        (item) => item.id === parseInt(req.params.id)
      );

      if (detailData.length === 0) {
        return res.status(404).json({ errors: ["Bank not found"] });
      }

      res.status(200).json({ data: detailData });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  addBanks(req, res, next) {
    try {
      let lastId = data[data.length - 1].id;

      data.push({
        id: lastId + 1,
        bank: req.body.bank,
        branch: req.body.branch,
        city: req.body.city,
      });

      res.status(201).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  updateBanks(req, res, next) {
    try {
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      if (!findData) {
        return res.status(404).json({ errors: ["Bank not found"] });
      }

      data = data.map((item) =>
        item.id === parseInt(req.params.id)
          ? {
              id: parseInt(req.params.id),
              bank: req.body.bank,
              branch: req.body.branch,
              city: req.body.city,
            }
          : item
      );

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  deleteBanks(req, res, next) {
    try {
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      if (!findData) {
        return res.status(404).json({ errors: ["Bank not found"] });
      }

      data = data.filter((item) => item.id !== parseInt(req.params.id));

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }
}

module.exports = new Bank ();
