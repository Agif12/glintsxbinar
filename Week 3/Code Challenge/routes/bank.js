const express = require("express"); 

const {
  getAllBanks,
  getDetailBanks,
  addBanks,
  updateBanks,
  deleteBanks,
} = require("../controllers/bank"); 

const router = express.Router(); 

router.get("/", getAllBanks);

router.get("/:id", getDetailBanks);

router.post("/", addBanks);

router.put("/:id", updateBanks);

router.delete("/:id", deleteBanks);

module.exports = router; 
