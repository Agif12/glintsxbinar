const express = require("express"); 
const app = express(); 
const banks = require("./routes/bank");

const port = process.env.PORT || 3000; // Define port

app.use(express.json()); 

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use("/bank", banks);


app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
