const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// function to calculate tube volume
const phi = 3.14;
function volumeTube (phi, jariJari, tinggi) {
    return phi * jariJari ** 2 * tinggi;
}

function inputJariJari () {
    rl.question (`Input jariJari yang kamu inginkan: `, (jariJari) => {
        if (!isNaN (jariJari)) {
            inputTinggi (jariJari);
        } else {
            console.log(`jariJari Harus Berbentuk Angka!!!\n`);
            inputJariJari ();
        }
    });
}

function inputTinggi (jariJari, tinggi) {
    rl.question (`Input Tinggi yang kamu inginkan: `, (tinggi) => {
        if (!isNaN (tinggi)) {
            console.log(`\nHasil Volume Tube: ${volumeTube(phi, jariJari, tinggi)}`);
            rl.close();
        } else {
            console.log(`Tinggi Harus Berbentuk Angka!!!\n`);
            inputTinggi (jariJari, tinggi);
        }
    }); 
}


console.log(`Volume Of Tube`);
console.log(`=========`);
inputJariJari();