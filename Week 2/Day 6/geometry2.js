const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});


const phi = 3.14;
const a = 1 / 3;

// function to calculate volume of cone
function volumeCone(a, phi, jariJari, tinggi) {
  return a * phi * jariJari ** 2 * tinggi;
}

function input() {
  rl.question("Input Jari-Jari Yang Kamu Inginkan: ", function (jariJari) {
    rl.question("Input Tinggi Yang Kamu Inginkan: ", function (tinggi) {
      if (jariJari > 0 && tinggi > 0) {
        console.log(
          `\n Volume Of Cone: ${volumeCone(a, phi, jariJari, tinggi)}`
        );
        rl.close();
      } else {
        console.log(`\nJari-Jari & Tinggi Harus Berbentuk Angka!!!`);
        input();
      }
    });
  });
}

console.log(`Volume Of Cone`);
console.log(`=========`);
input();