const EventEmitter = require("events"); // Import event
const readline = require("readline"); // Import readline
const pasienCovid = require("../Day 9/number2");

// Make event instance
const my = new EventEmitter();
// Make readline
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Listener
my.on("login:failed", function (email) {
  console.log(`${email} is failed to login!`);
  rl.close();
});

my.on("login:success", function (email) {
  console.log(`${email} is success to login!`);
  rl.question(`Input data yang ingin anda cek: `, (cek) => {
    if (cek === "Positive") {
      pasienCovid("Positive");
      rl.close();
    } else if (cek === "Negative") {
      pasienCovid("Negative");
      rl.close();
    } else if (cek === "Suspect") {
      pasienCovid("Suspect");
      rl.close();
    } else {
      console.log("Data yang ingin anda cek tidak tersedia.");
      rl.close();
    }
  });
});

// Function to login
function login(email, password) {
  const passwordStoredInDatabase = "135790";

  if (password !== passwordStoredInDatabase) {
    my.emit("login:failed", email); // Pass the email to the listener
  } else {
    // Do something
    my.emit("login:success", email);
  }
}

// Input email and password
rl.question("Email: ", function (email) {
  rl.question("Password: ", function (password) {
    login(email, password); // Run login function
  });
});
